package com.ming.kitty.admin.service;

import com.ming.kitty.admin.model.SysUser;

import java.util.List;

public interface SysUserService {
    SysUser findByUserId(Long userId);
    List<SysUser> findAll();
}
